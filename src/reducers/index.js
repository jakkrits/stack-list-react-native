/**
 * Created by jakkrit on 9/27/2016 AD.
 */

import {combineReducers} from 'redux';

const libraryReducer = (state = [], action) => {
    switch(action.type) {
        case 'SHOW_LIBRARY': return [...state, ...action.data];
    }
    return state;
};

const selectionReducer = (state = null, action) => {
    switch(action.type) {
        case 'SELECT_LIBRARY':
            return action.librayID;
    }
    return state;
};

export default combineReducers({
    libraries: libraryReducer,
    selectedID: selectionReducer
});