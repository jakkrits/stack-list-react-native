/**
 * Created by Jakk on 9/27/2016 AD.
 */
import React, {Component} from 'react';
import {
    Text,
    TouchableWithoutFeedback,
    View,
    LayoutAnimation
} from 'react-native';
import {connect} from 'react-redux';
import {CardSection} from './common';
import * as actions from '../actions';

class ListItem extends Component {
    constructor(props) {
        super(props);
        this.renderDescription = this.renderDescription.bind(this);
    }

    componentWillUpdate() {
        LayoutAnimation.spring();
    }

    renderDescription() {
        const {library, selectedID} = this.props;
        if (selectedID === library.id) {
            return <CardSection>
                <Text style={styles.descriptionStyle}>{library.description}</Text>
            </CardSection>
        }

    }

    render() {
        const {titleStyle} = styles;
        const {id, title} = this.props.library;
        return (
            <TouchableWithoutFeedback onPress={() => this.props.selectLibrary(id)}>
                <View>
                    <CardSection>
                        <Text style={titleStyle}>
                            {title}
                        </Text>
                    </CardSection>
                    {this.renderDescription()}
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = {
    titleStyle: {
        fontSize: 16,
        paddingLeft: 15
    },
    descriptionStyle: {
        flex: 1,
        padding: 10
    }
};

function mapStateToProps(state, ownProps) {
    console.log(`this is ownProps of ListItem: ${ownProps}`);
    return {
        selectedID: state.selectedID
    }
}
export default connect(mapStateToProps, actions)(ListItem);