/**
 * Created by jakkrit on 9/27/2016 AD.
 */

import data from '../reducers/LibraryList.json';

export const showLibrary = {
    type: 'SHOW_LIBRARY',
    data
};

export const selectLibrary = (librayID) => {
    return {
        type: 'SELECT_LIBRARY',
        librayID
    };
};

