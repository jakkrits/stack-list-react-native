/**
 * Created by Jakk on 9/27/2016 AD.
 */
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import reducers from './reducers';
import {Header} from './components/common';
import LibraryList from './components/LibraryList';
import * as actions from './actions';

const store = createStore(reducers);
store.dispatch(actions.showLibrary);

export default class App extends Component {
    render() {
        const {rootViewStyle} = styles;
        return(
            <Provider store={store}>
                <View style={rootViewStyle}>
                    <Header headerText="React Redux StackList"/>
                    <LibraryList/>
                </View>
            </Provider>
        );
    }
}

const styles = {
  rootViewStyle: {
      flex: 1
  }
};